<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Lib\Model;

/**
 * Description of ModelTrigger
 *
 * @author ermanno
 */
abstract class ModelTrigger {
    abstract public function exec($model);
}
