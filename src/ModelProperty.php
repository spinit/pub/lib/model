<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Lib\Model;

use Spinit\Util;
use Spinit\Lib\Model\Model;

/**
 * Description of ModelProperty
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
class ModelProperty
{
    /**
     *
     * @var Model
     */
    private $model;
    private $values;
    private $trace = '';
    private $property = '';
    public function __construct(Model $model)
    {
        $this->model = $model;
        $this->values = false;
        $this->property = $model->getResource().'__p';
        $this->trace = $this->property.'x';
        $this->aligned = false;
    }
    public function getResource()
    {
        return $this->property;
    }
    public function clear()
    {
        $this->values = false;
    }
    
    public function align()
    {
        if ($this->aligned) {
            return;
        }
        if (!$this->model->getDataSource()->check($this->property)) {
            $this->model->getDataSource()->align($this->getPropertyDataStruct());
            $this->model->getDataSource()->align($this->getTraceDataStruct());
        }
        $this->aligned = true;
    }
        
    private function getPropertyDataStruct()
    {
        return [
            'name' => $this->property,
            'fields' => [
                'id' => ['type' => 'uuid', 'pkey' => '1'],
                'id_mst' => ['type' => 'uuid'],
                'id_prp' => ['type' => 'uuid'],
                'nme_prp' => ['type' => 'varchar', 'size'=>'256'],
                'typ' => ['type' => 'integer'],
                'ctx' => ['type' => 'varchar'],
                'val_ref' => ['type' => 'uuid'],
                'val_str' => ['type' => 'text'],
                'dat_ins__' => ['type' => 'datetime'],
                'usr_ins__' => ['type' => 'uuid'],
            ]
        ];
    }
        
    private function getTraceDataStruct()
    {
        return [
            'name' => $this->trace,
            'fields' => [
                'id' => ['type' => 'uuid', 'pkey' => '1'],
                'id_mst' => ['type' => 'uuid'],
                'id_prp' => ['type' => 'uuid'],
                'nme_prp' => ['type' => 'varchar', 'size'=>'256'],
                'typ' => ['type' => 'integer'],
                'ctx' => ['type' => 'varchar'],
                'val_ref' => ['type' => 'uuid'],
                'val_str' => ['type' => 'text'],
                'dat_ins__' => ['type' => 'datetime'],
                'usr_ins__' => ['type' => 'uuid'],
                'dat_opr__' => ['type' => 'datetime'],
                'usr_opr__' => ['type' => 'uuid'],
                'dat_del__' => ['type' => 'datetime'],
                'usr_del__' => ['type' => 'uuid'],
            ]
        ];
    }
    
    public function setValue($pid, $value, $field = '', $type = '')
    {
        // se pid è un esadecimale ... non viene convertito
        if(preg_match('/^([0-9A-Fa-f])+$/', $pid)) {
            $key = strtoupper($pid);
        } else {
            $key = strtoupper(md5($pid));
        }
        if ($this->values === false) {
            $this->values = $this->loadIdMap();
        }
        if ($field) {
            $vv = new Util\Dictionary(@$this->values[$key]['value']?:[]);
            $vv->set($field, $value);
            $value = $vv->asArray();
        }
        if (is_array($value)) {
            $type = 'json';
        }
        $this->values[$key]['value'] = $value;
        $this->values[$key]['name'] = $pid;
        $this->values[$key]['type'] = Util\nvl($type, Util\arrayGet($this->values, [$key, 'type']));
    }
    
    public function getValue($pid, $field = '')
    {
        if(preg_match('/^([0-9A-Fa-f])+$/', $pid)) {
            $key = strtoupper($pid);
        } else {
            $key = strtoupper(md5($pid));
        }
        if ($this->values === false) {
            $this->values = $this->loadIdMap();
        }
        $value = Util\arrayGet($this->values, [$key, 'value']);
        if ($field) {
            $value = Util\arrayGet($value, Util\asArray($field, '.'));
        }
        return $value;
    }
    
   
    public function sync()
    {
        // se i valori non sono inizializzati ... allora non viene fatta nessuna operazione
        if ($this->values === false) {
            return;
        }
        $prps = $this->loadIdMap();
        // aggiorna e inserisce i valori non presenti nel DB
        foreach($this->values as $id_prp => $data) {
            $rec = Util\arrayGet($prps, $id_prp);
            // record trovato .. viene tolto per non riconsiderarlo
            if (is_array($prps)?array_key_exists($id_prp, $prps):isset($id_prp, $prps)) {
                if ($prps instanceof \ArrayObject) {
                    if ($prps->offsetExists($id_prp)) $prps->offsetUnset($id_prp);
                } else {
                    unset($prps[$id_prp]);
                }
            }
            // se il valore non è cambiato non occorre modificare nulla
            if ($rec and $data['value'] == Util\arrayGet($rec, 'value')) {
                continue;
            }
            // il valore è cambiato 
            // se è presente nel DB -> viene spostato in trace e ... 
            $this->recMove($rec);
            // viene inserito un nuovo record
            $this->dataInsert($id_prp, $data);
        }
    }
    
    private function loadIdMap()
    {
        $prps = new \ArrayObject();
        $this->align();
        $pval = $this->getPVal();
        // proprietà effettive
        foreach($this->model->getDataSource()->select($this->property, ['id_mst'=>$pval], 'hex(id) as id, typ, hex(id_prp) as id_prp, hex(val_ref) val_ref, val_str, nme_prp') as $prp) {
            $key = strtoupper($prp['id_prp']);
            switch($prp['typ']) {
                case 1:
                    $prps[$key] = ['type'=>'uuid', 'value'=>$prp['val_ref'], 'id'=> $prp['id'], 'name'=>$prp['nme_prp']];
                    break;
                case 2:
                    $prps[$key] = ['type'=>'json', 'value'=>json_decode($prp['val_str'], 1), 'id'=> $prp['id'], 'name'=>$prp['nme_prp']];
                    break;
                default:
                    $prps[$key] = ['type'=>'string', 'value'=>$prp['val_str'], 'id'=> $prp['id'], 'name'=>$prp['nme_prp']];
                    break;
            }
        }
        return $prps;
    }
    
    private function getFieldType($name)
    {
        switch ($name) {
            case 'uuid':
                return ['val_ref', 1];
            case 'json':
                return ['val_str', 2];
            default : 
                return ['val_str', 0];
        }
    }
    
    private function recMove($rec, $isDelete = false) 
    {
        if (! Util\arrayGet($rec, 'id')) {
            return;
        }
        $delVals = $isDelete ? 'now(), {{@user}}' : 'null, null';
        $DM = $this->model->getDataSource();
        $fieldTr = implode(', ', array_keys($this->getTraceDataStruct()['fields']));
        $fieldRs = implode(', ', array_keys($this->getPropertyDataStruct()['fields']));
        $sql = 'INSERT INTO '.$this->trace.' ('.$fieldTr.')'.PHP_EOL.
               ' SELECT '.' '.$fieldRs.', now(), {{@user}}, '.$delVals.PHP_EOL.
               ' FROM '.$this->property.PHP_EOL.
               ' WHERE id = {{@id}}';
        $DM->exec($sql, ['user'=>$DM->getAdapter()->getParam('userTrace'), 'id'=>Util\arrayGet($rec, 'id')]);
        $DM->delete($this->property, Util\arrayGet($rec, 'id'));
    }
    
    private function dataInsert($id_prp, $data)
    {
        $DM = $this->model->getDataSource();
        $value = Util\arrayGet($data, 'value');
        if (is_array($value)) {
            list($field, $typ) = $this->getFieldType('json');
        } else {
            list($field, $typ) = $this->getFieldType(Util\arrayGet($data, 'type'));
        }
        $value = ($typ == 2 ) ? json_encode($value, JSON_PRETTY_PRINT) : $value;
        if (strlen($value)) {
            $DM->insert(
                $this->property, 
                [
                    'id' => $DM->getCounter()->next(),
                    'id_mst' => $this->getPVal(),
                    'id_prp'=>$id_prp,
                    'nme_prp'=>Util\arrayGet($data, 'name'),
                    'typ'=>$typ,
                    $field => $value,
                ]
            );
        }
    }
    
    public function delete()
    {
        $pval = $this->getPVal();
        if ($this->model->getDataSource()->check($this->property)) {
            $this->model->getDataSource()->delete($this->property,['id_mst' => $pval]);
            $this->model->getDataSource()->delete($this->trace,['id_mst' => $pval]);
        }
    }
    
    private function getPVal()
    {
        $pkey = $this->model->getPkey() ?: [];
        return is_array($pkey) ? array_shift($pkey) : $pkey;
    }
}
