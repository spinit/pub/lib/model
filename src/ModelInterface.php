<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Lib\Model;

/**
 * Description of Model
 *
 * @author ermanno
 */
interface ModelInterface {
    public function getPkey();
    public function save();
    public function load($key);
    public function delete();
    public function get($name);
    public function set();
}
