<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Lib\Model\FieldType;

use Spinit\Lib\Model\FieldTypeInterface;
use Spinit\Lib\Model\Model;
use Spinit\Util;
use Spinit\Lib\DataSource\StoreValue;

/**
 * Description of Model
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */

class ValueType implements FieldTypeInterface
{
    public function format($value, $opt)
    {
        return $value;
    }

    public function check($value, $opt, $oldValue)
    {
        return $value;
    }
    public function serialize(Model $model, $value, $field)
    {
        switch(Util\arrayGet($field, 'type')) {
            case 'date':
            case 'datetime':
            case 'integer':
            case 'int':
                if ($value === '') {
                    return null;
                }
        }
        if ($apply = Util\arrayGet($field, 'apply')) {
            return new StoreValue($apply, $value);
        }
        return $value;
    }
    public function getTypeName()
    {
        return '';
    }
    /**
     * conversione database -> model
     * @param type $value
     * @return type
     */
    public function decode($value)
    {
        return $value;
    }
}
