<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Lib\Model\FieldType;
use Spinit\Lib\Model\Model;
use Spinit\Util;
use Spinit\UUIDO;
use Spinit\Lib\DataSource\PDO\AdapterPDO;
/**
 * Description of IncrementType
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
class UuidType extends ValueType
{
    public function check($value, $opt, $oldValue)
    {
        if ($value === '') {
            return null;
        }
        return parent::check($value, $opt, $oldValue);
    }
    public function serialize(Model $model, $value, $field)
    {
        $manager = $model->getDataSource();
        if (!$value and Util\arrayGet($field, 'pkey')) {
            $size = Util\arrayGet($field, 'size');
            if ($size and $size < AdapterPDO::$UUID_SIZE) {
                $value = UUIDO\randhex($size*2);
            } else {
                $value = (string) $manager->getCounter()->next();
            }
            return $value;
        }
        return parent::serialize($model, $value, $field);
    }
    public function getTypeName()
    {
        return 'uuid';
    }
    public function decode($value)
    {
        return strtoupper($value);
    }
}
