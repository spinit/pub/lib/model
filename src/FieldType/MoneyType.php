<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Lib\Model\FieldType;
use Spinit\Lib\Model\FieldTypeInterface;
use Spinit\Lib\Model\Model;

/**
 * Description of IncrementType
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
class MoneyType extends ValueType
{
    public function check($value, $opt, $oldValue)
    {
        if ($value === '') {
            return null;
        }
        return parent::check($value, $opt, $oldValue);
    }
    public function serialize(Model $model, $value, $field)
    {
        if ($value === '') {
            return null;
        }
        return parent::serialize($model, $value, $field);
    }
    public function getTypeName()
    {
        return 'money';
    }
    public function decode($value)
    {
        if (trim($value) === '') {
            return '';
        }
        return number_format($value, 2, '.', '');
    }
}
