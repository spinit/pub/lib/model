<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Lib\Model\FieldType;

use Spinit\Lib\Model\FieldType;
use Spinit\Lib\Model\Model;

/**
 * Description of ModelType
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
class ModelType extends FieldType
{
    
    public function getTypeName()
    {
        return "model";
    }

    public function serialize(Model $model, $value, $field)
    {
        
    }

}
