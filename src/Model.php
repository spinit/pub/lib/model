<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Lib\Model;

use Spinit\Lib\Model\ModelInterface;
use Spinit\Util;
use Webmozart\Assert\Assert;
use Spinit\Lib\DataSource\DataSourceInterface;
use Spinit\Lib\Model\Adapter\ModelAdapter;
/**
 * Description of Model
 *
 * @author ermanno
 */
class Model extends Util\Dictionary implements ModelInterface {
    
    private $adapter;
    private $datasource;
    private $pkey;
    private $data;
    
    static protected $types = [];

    private $property;    
    
    public function __construct(ModelAdapter $adapter, DataSourceInterface $datasource) {
        parent::__construct();
        
        $this->adapter = $adapter;
        $this->datasource = $datasource;
        $this->data = [];
        $this->initEvent();
        $this->struct = $adapter->getStruct();
        if ($this->getAdapter()->get('hasProperty')) {
            $this->property = new ModelProperty($this);
        }
    }
    public static function setType($name) {
        $args = func_get_args();
        array_shift($args);
        if (is_array($name)) {
            foreach($name as $k => $v) {
                self::setType($k, $v);
            }
            return;
        }
        $class = trim(Util\getColonsPath(array_shift($args)));
        if (!$class ) {
            unset (self::$types[$name]);
        } else {
            self::$types[$name] = Util\getInstance($class);
        }
    }
    public static function getType($name) {
        return Util\arrayGet(self::$types, $name, function() {
            return self::getType('default');
        });
    }
    private function getProperty() {
        Assert::notNull($this->property, 'Tabellella proprietà non impostata per '.$this->getAdapter()->getStruct()['name']);
        return $this->property;
    }
    public function getPropertyValue() {
        return call_user_func_array([$this->getProperty(), 'getValue'], func_get_args());
    }
    public function setPropertyValue() {
        return call_user_func_array([$this->getProperty(), 'setValue'], func_get_args());
    }
    
    private function initEvent() {
        $this->bindExec('setValue', function($event) {
            parent::set($event->getParam(0), $event->getParam(1));
        });
        $this->bindExec('load', function($event) {
            $rec = $this->datasource->select($this->adapter->getResource(), $event->getParam(0))->first();
            if ($rec) {
                foreach($this->struct['fields'] as $field) {
                    if ($field['type'] == 'json') {
                        $rec[$field['name']] = json_decode($rec[$field['name']], 1);
                    }
                }
                $this->set($rec);
                $this->setPkey($event->getParam(0));
            }
        });
        $this->bindAfter('load', function($event) {
            $this->data = $this->asArray() ?: [];
        });
        $this->bindExec('insert', function($event) {
            $this->getDataSource()->insert($this->getAdapter()->getStruct()['name'], $this->asArray());
        }); 
        $this->bindExec('update', function($event) {
            $sucData = $this->asArray();
            $pkey = $event->getParam(0);
            $preData = $event->getParam(1);
            
            $list = [];
            foreach($sucData as $k=>$v) {
                $notHas = ! (is_array($preData)?array_key_exists($k, $preData):isset($k, $preData));
                if ($notHas or $preData[$k] !== $v) {
                    $field = Util\arrayGetAssert($this->struct['fields'], $k, 'Campo non trovato : '.$k);
                    $type = self::getType($field['type']);
                    $list[$k] = $type->serialize($this, $v, $field);
                }
            }
            if (count($list)) {
                $this->getDataSource()->update($this->getAdapter()->getStruct()['name'], $list, $pkey);
            }
        }); 
        $this->bindExec('save', function ($event) {
            $fnc = $event->getParam(0) ?: function () {};
            if (!$this->pkey) {
                $pkey = [];
                foreach($this->struct['fields'] as $name => $field) {
                    try {
                        $type = self::getType($field['type']);
                        $value = $type->serialize($this, $this->get($name), $field);
                        if (Util\arrayGet($field, 'pkey')) {
                            $pkey[$name] = $value;
                        }
                        $this->set($name, $value);
                    } catch (\Exception $ex) {
                        debugx([$field, $ex->getMessage(), $this->asArray(), $this->getResource()]);
                        throw $ex;
                    }
                }
                $this->trigger('insert');
                if (count($pkey)) {
                    $this->setPkey($pkey);
                }
                $fnc('insert', $this);
            } else {
                $this->trigger('update', [$this->pkey, $this->data]);
                $fnc('update', $this);
            }
        });
        $this->bindEnd('save', function($event) {
            if ($this->property) {
                $this->property->sync();
            }
        });
        $this->bindAfter('update', function($event) {
            $this->setPkey($event->getParam(0));
            $this->data = $this->asArray() ?: [];
        });
        $this->bindExec('delete', function($event) {
            $this->getDataSource()->delete($this->getAdapter()->getStruct()['name'], $event->getParam(0));
        });
        $this->bindAfter('delete', function($event) {
            $this->clear();
        });
        
    }
    public function initTypes() {
        
    }
    /**
     * Vengono eseguiti i trigger definiti sul model
     * @param type $event
     * @param type $target
     */
    public function onTrigger($event, $target) {
        foreach($this->getAdapter()->getTriggerList($event['name'], $event['when']) as $trigger) {
            $trigger->exec($this);
        } 
        
    }
    public function getResource() {
        return $this->getAdapter()->getResource();
    }
    public function getAdapter() {
        return $this->adapter;
    }
    
    public function getDataSource() {
        return $this->datasource;
    }
    
    public function getStruct() {
        $struct = $this->getAdapter()->getStruct();
        return $struct;
    }
    
    public function init() {
       
        $this->getDataSource()->align($this->getStruct(), function($event, $ADS) {
            
            // se l'allineamento genera una creazione ... allora si 
            // richiede l'inserimento degli eventuali elementi di inizializzazione
            if ($event == 'create') {
                $this->getAdapter()->iterInitList(function ($item) {
                    $this->clear();
                    $this->set(array_change_key_case($item, CASE_LOWER));
                    $this->save();
                });
            }
            
        });
        if ($this->property) $this->property->align();
        return $this;
    }
    public function set() {
        $args = func_get_args();
        $name = array_shift($args);
        if (is_array($name)) {
            Assert::count($args, 0);
            foreach($name as $k => $v) {
                $this->trigger('setValue', [$k, $v]);
            }
        } else {
            $this->trigger('setValue', [$name, array_shift($args)]);
        }
        return $this;
    }
    
    public function setPkey($pkey) {
        $this->pkey = $pkey;
        return $this;
    }
    
    public function getPkey() {
        return $this->pkey;
    }
    
    public function clear() {
        while(count($this)) $this->shift();
        $this->data = new Util\Dictionary();
        $this->pkey = null;
        $this->property = null;
        if ($this->getAdapter()->get('hasProperty')) {
            $this->property = new ModelProperty($this);
        }
    }
    public function load($pkey) {
        try {
            $this->clear();
            $this->trigger('load', [$pkey]);
        } catch (Util\Error\NotFoundException $e) {
            $this->setPkey(null);
        }
        return $this;
    }
    
    public function save($fnc = null) {
        $this->trigger('save', [$fnc]);
        return $this;
    }
    
    public function delete($pkey = null) {
        $pkey = Util\nvl($pkey, $this->pkey);
        if ($pkey) {
            $this->trigger('delete', [$pkey]);
        }
        return $this;
    }
}

Model::setType('default', __NAMESPACE__.':FieldType:ValueType');
