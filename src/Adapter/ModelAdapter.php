<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Lib\Model\Adapter;

/**
 * Description of ModelAdapter
 *
 * @author ermanno
 */
abstract class ModelAdapter {
    private $name;
    public function __construct($name)
    {
        $this->name = $name;
    }
    public function getName()
    {
        return $this->name;
    }
    abstract public function getStruct();
    abstract public function getResource();
    abstract public function getSource();
    abstract public function iterInitList($iterator);
    //abstract public function getTriggerList();
}
