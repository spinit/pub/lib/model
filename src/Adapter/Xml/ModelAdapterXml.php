<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Lib\Model\Adapter\Xml;

use Spinit\Lib\Model\Adapter\ModelAdapter;
use Spinit\Util;
use Webmozart\Assert\Assert;
/**
 * Description of ModelAdapterXml
 *
 * @author ermanno
 */

class ModelAdapterXml extends ModelAdapter {
    private $root;
    private $fname;
    private $index;
    private $trigger;
    public function __construct($name, $file) {
        parent::__construct($name);
        Assert::file($file);
        $this->fname = $file;
        $this->root = dirname($file);
        $this->index = simplexml_load_file($this->fname, null, LIBXML_NOCDATA);
        Assert::notEmpty($this->index, 'Errore di sintassi in : '.realpath($file));
        $this->trigger = [];
        if ($this->index->trigger) {
            foreach($this->index->trigger->event?:[] as $trigger) {
                $this->trigger[(string)$trigger['name']][(string)$trigger['when']][] = new ModelTriggerXml($trigger);
            }
        }
    }
    public function getStruct() {
        $data = [
            'name' => $this->get('resource'),
            'trace' =>  $this->get('trace')
        ];
        foreach ($this->index->struct->field?:[] as $field) {
            $attr = Util\arrayGet((array) $field, '@attributes', []);
            if (Util\arrayGet($attr, 'name')) {
                $data['fields'][$attr['name']] = $attr;
            }
        }
        $data['exec'] = [];
        foreach($this->index->exec as $exec) {
            $data['exec'][] = json_decode(json_encode($exec), 1);
        }
        return $data;
    }

    public function getResource() {
        return (string) $this->index['resource'];
    }

    public function getSource() {
        return (string) $this->index['source'];
    }
    public function get($name) {
        return (string)$this->index[$name];
    }
    public function iterInitList($iterator) {
        if (!$this->index->init) {
            return;
        }
        $fname = $this->root.'/'.(string)$this->index->init['data'];
        try {
            $sax = new ParserSax($fname);
            $sax->parse($iterator);
        } catch (\Exception $ex) {

        }
    }
    public function getTriggerList($name, $when) {
        return Util\arrayGet($this->trigger, [$name, $when], []);
    }
}
