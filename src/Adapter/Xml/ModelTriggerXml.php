<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Lib\Model\Adapter\Xml;

use Spinit\Lib\Model\ModelTrigger;
use Webmozart\Assert\Assert;
use Spinit\Util;

/**
 * Description of ModelTriggerXml
 *
 * @author ermanno
 */
class ModelTriggerXml extends ModelTrigger {
    
    private $trigger;
    
    public function __construct($trigger) {
        $this->trigger = $trigger;
    }
    public function exec($model) {
        foreach($this->trigger->children() as $child) {
            $method = 'cmd'.ucfirst($child->getName());
            Assert::methodExists($this, $method);
            call_user_func([$this, $method], $model, $child);
        }
    }
    
    protected function cmdSet($model, $child) {
        $field = (string) $child['field'];
        Assert::notEmpty($field, 'nome campo non impostato');
        list($value, ) = Util\normalize((string) $child, $model->asArray());
        $model->set($field, $value);
    }
}
