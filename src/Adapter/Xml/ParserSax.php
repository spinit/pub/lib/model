<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Lib\Model\Adapter\Xml;

use Spinit\Util\Error\NotFoundException;
/**
 * Description of ParserSax
 *
 * @author ermanno
 */
class ParserSax {

    var $prc;
    var $fname;
    var $dep = 0;
    var $tag = '';
    var $callRecord = null;
    
    public function __construct($fname) {
        
        $this->fname = $fname;
        if (!is_file($this->fname)) {
            throw new NotFoundException($this->fname);
        }
        $this->prc = xml_parser_create();
        xml_set_object($this->prc, $this);
        xml_set_element_handler($this->prc, 'start_element', 'end_element');
        xml_set_character_data_handler($this->prc, 'character_data');
        xml_parser_set_option($this->prc, XML_OPTION_CASE_FOLDING, false);
    }

    public function parse($callRecord) {
        $fp = fopen($this->fname, 'r');
        $this->callRecord = $callRecord;
        if (!$fp) {
            throw new \Exception('Errore nella lettura del file : '.$this->fname);
        }
        while ($data = fread($fp, 4096)) {
            if (!xml_parse($this->prc, $data, feof($fp))) {
                fclose($fp);
                throw new \Exception('Errore durante il parsing : '.$this->fname);
            }
        } 
        fclose($fp);
    }

    function start_element($parser, $tag, $attributes) {
        $this->dep += 1;
        switch($this->dep) {
            case 1:
                return;
            case 2:
                $this->item = [];
                return;
            case 3:
                $this->tag = $tag;
            default:
                return;
        }
    }

    function end_element($parser, $tag) {
        if ($this->dep == 2) {
            call_user_func($this->callRecord, $this->item);
            $this->item  = [];
            $this->tag = '';
        }
        $this->dep -= 1;
    }

    function character_data($parser, $data) {
        if ($this->dep == 3 and $this->tag) {
            if (!isset($this->item[$this->tag])) {
                $this->item[$this->tag] = '';
            }
            $this->item[$this->tag] .= $data;
        } 
    }

}
