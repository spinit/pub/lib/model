<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Lib\Model;

use Spinit\Lib\Model\Model;

/**
 * Description of Model
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */

interface FieldTypeInterface
{
    public function format($value, $opt);
    public function check($value, $opt, $oldValue);
    public function serialize(Model $model, $value, $field);
    public function getTypeName();
}
