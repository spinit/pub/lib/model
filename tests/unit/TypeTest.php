<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Lib\Model;

use PHPUnit\Framework\TestCase;
use Spinit\Lib\Model\Tests\DatasourceMoch;
use Spinit\Lib\Model\Model;
use Spinit\Util;
/**
 * Description of ModelTest
 *
 * @author ermanno
 */
class TypeTest extends TestCase {
    
    private $object;
    
    protected function setUp() : void {
        $this->object = new Model(new Adapter\Xml\ModelAdapterXml('ModelTest', __DIR__.'/ModelTest.xml'), 
                                  new Tests\DataSourceMoch());
    }
    public function testChar() {
        $this->object->getDataSource()
             ->setCaller(['getCounter'=>function() {
                 return $this;
            }]);
        $t1 = Model::getType('uuid');
        $this->assertNotNull($t1);
        $val = $t1->serialize($this->object, '', ['pkey'=>'1']);
        $this->assertEquals('hello', $val);
    }
    
    public function next() {
        return 'hello';
    }
}
