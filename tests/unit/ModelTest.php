<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Lib\Model;

use PHPUnit\Framework\TestCase;
use Spinit\Lib\Model\Tests\DatasourceMoch;
use Spinit\Lib\Model\Model;
use Spinit\Util;
/**
 * Description of ModelTest
 *
 * @author ermanno
 */
class ModelTest extends TestCase {
    private $object;
    
    protected function setUp() : void {
        $this->object = new Model(new Adapter\Xml\ModelAdapterXml('ModelTest', __DIR__.'/ModelTest.xml'), 
                           new Tests\DataSourceMoch());
    }
    public function testModel() {
        $this->object['uno'] = 'due';
        $this->object['uno.due'] = 'tre';
        $this->assertEquals($this->object['uno'], ['due'=>'tre']);
    }
    
    public function testDataSource(){
        $data = new \stdClass();
        $this->object->getDataSource()->setCaller(['align'=>function($struct) use ($data) {
            $data->struct = $struct;
        }]);
        $this->object->init();
        $this->assertEquals('test_resource', $data->struct['name']);
        $this->assertCount(2, $data->struct['fields']);
    }
    
    public function testInsert() {
        $data = new \stdClass();
        $this->object->getDataSource()->setCaller(['insert'=>function($resource, $rec) use ($data) {
            $data->rec = $rec;
        }, 'getCounter'=>function() {
            return $this;
        }]);
        $this->object['uno'] = ['due' => 'tre'];
        $this->object->save();
        $this->assertEquals($data->rec, $this->object->asArray());
        $this->assertEquals('111', $data->rec['id']);
    }
    public function next() {
        return '111';
    }
    public function testUpdate() {
        $data = new \stdClass();
        $this->object->getDataSource()->setCaller([
            'update'=>function($resource, $rec, $pkey) use ($data) {
                $data->rec = $rec;
                $data->pkey = $pkey;
            },
            'select' => function($resource, $pkey, $field = '') {
                return new Util\Dictionary(['first'=>function() {
                    return ['uno'=>'1'];
                }]);
            }
        ]);
        $this->object->load(['id'=>'4']);
        $this->assertEquals(['id'=>'4'], $this->object->getPkey());
        
        $this->object['uno'] = ['due' => 'tre'];
        $this->object->save();
        $this->assertEquals(json_decode($data->rec['uno'], 1), $this->object->asArray()['uno']);
        $this->assertEquals($data->pkey, $this->object->getPkey());
    }
}
