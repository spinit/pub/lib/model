<?php

use PHPUnit\Framework\TestCase;

use Spinit\Lib\Model\Adapter\Xml\ParserSax;
use Spinit\Util\Error\NotFoundException;
/**
 * Description of ParserSaxTest
 *
 * @author ermanno
 */
class ParserSaxTest extends TestCase {
    
    public function testEmpty() {
        try {
            $obj = new ParserSax('');
            $this->fail('file senza nome non dovrebbe esistere');
        } catch (NotFoundException $e) {
            $this->assertEquals('', $e->getMessage());
        }
    }
    public function testBooo() {
        try {
            $obj = new ParserSax(__DIR__.'/parse-unk.xml');
            $this->fail('il file "'.__DIR__.'/parse-unk.xml'.'" non ci dovrebbe essere');
        } catch (NotFoundException $e) {
            $this->assertEquals(__DIR__.'/parse-unk.xml', $e->getMessage());
        }
    }
    public function testFielOk() {
        $obj = new ParserSax(__DIR__.'/data.xml');
        $dataProvider = [
            ['id'=>1, 'name'=>'test'],
            ['id'=>2, 'name'=>'prova'],
        ];
        $list = $obj->parse(function($item) use (&$dataProvider) {
            $current = array_shift($dataProvider);
            $this->assertEquals($current, $item);
        });
    }
}
