<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Lib\Model\Tests;

use Spinit\Lib\DataSource\DataSourceInterface;
use Spinit\Util;

/**
 * Description of ModelMoch
 *
 * @author ermanno
 */
class DataSourceMoch implements DataSourceInterface {
    
    private $caller;
    public function setCaller($caller) {
        if (is_array($caller)) {
            $caller = new Util\Dictionary($caller);
        }
        $this->caller = $caller;
    }
    public function align($resourceStruct) {
        return call_user_func_array([$this->caller, 'align'], func_get_args());
    }

    public function check($resourceName) {
        return call_user_func_array([$this->caller, 'check'], func_get_args());
    }

    public function connect($conString) {
        return call_user_func_array([$this->caller, 'connect'], func_get_args());
    }

    public function delete($resource, $key) {
        return call_user_func_array([$this->caller, 'delete'], func_get_args());
    }

    public function drop($resourceName) {
        return call_user_func_array([$this->caller, 'drop'], func_get_args());
    }

    public function exec($cmd) {
        return call_user_func_array([$this->caller, 'exec'], func_get_args());
    }

    public function insert($resource, $data) {
        return call_user_func_array([$this->caller, 'insert'], func_get_args());
    }

    public function query($cmd) {
        return call_user_func_array([$this->caller, 'query'], func_get_args());
    }

    public function truncate($resourceName) {
        return call_user_func_array([$this->caller, 'truncate'], func_get_args());
    }

    public function update($resource, $data, $key) {
        return call_user_func_array([$this->caller, 'update'], func_get_args());
    }
    public function select($resource, $key, $fields = '') {
        return call_user_func_array([$this->caller, 'select'], func_get_args());
    }
    public function getPkeyDefault() {
        return 'id';
    }
    public function getCounter() {
        return call_user_func_array([$this->caller, 'getCounter'], func_get_args());
    }
    public function getConnectionString() {
        return "";
    }
}
