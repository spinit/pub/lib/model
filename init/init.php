<?php
namespace Spinit\Lib\Model;

use Spinit\Lib\Model\Model;

Model::setType([
    'money'     => __NAMESPACE__.':FieldType:MoneyType',
    'uuid'      => __NAMESPACE__.':FieldType:UuidType',
    'json'      => __NAMESPACE__.':FieldType:JsonType',
    'increment' => __NAMESPACE__.':FieldType:IncrementType',
    //'model'     => __NAMESPACE__.':FieldType:ModelType'
]);
